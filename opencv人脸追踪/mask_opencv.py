import cv2

cap = cv2.VideoCapture('F:/opencvvideo/writevideo_source.avi')  #这里填入要处理的视频的绝对路径
face_cascade = cv2.CascadeClassifier(r'F:/opencv/opencv/sources/data/haarcascades/haarcascade_frontalface_default.xml')
fourcc = cv2.VideoWriter_fourcc(*'XVID')  #这里设定相应的编码格式
out = cv2.VideoWriter('F:/opencvvideo/outvideo_mask1.avi', -1, 10.0, (640, 480))   #这里设定输出处理过的视频的位置、分辨率、fps

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret != True:
        break
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, scaleFactor = 1.15, minNeighbors = 5, minSize = (5, 5))
    for(x, y, w, h) in faces:
        gray1 = cv2.blur(gray, (w, h))
        cv2.rectangle(gray, (x, y), (x + w, y + h), (220, 20, 60), 8)
        gray1 = cv2.resize(gray1, (w, h))
        gray[y:y+w,x:x+h] = gray1

    out.write(gray)
    cv2.imshow('OutVideo', gray)
    if cv2.waitKey(30) == 27 & 0xFF == ord('q'):
        break
 
cap.release()
cv2.destroyAllWindows()
