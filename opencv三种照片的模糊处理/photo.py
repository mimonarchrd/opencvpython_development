import cv2

def main():
    image = cv2.imread("F:\\visual_studio_2013_Projects\\first\\first\\lena.jpg")
    cv2.namedWindow("source", 1)
    cv2.imshow("source", image)
    dst = cv2.blur(image, (1, 15))
    cv2.namedWindow('blur_demo', cv2.WINDOW_NORMAL)
    cv2.imwrite("F:\\visual_studio_2013_Projects\\first\\first\\blur_demo.jpg", dst)
    cv2.imshow("blur_demo", dst)
    dst1 = cv2.medianBlur(image, 5)
    cv2.namedWindow('median_blur_demo', cv2.WINDOW_NORMAL)
    cv2.imwrite("F:\\visual_studio_2013_Projects\\first\\first\\median_blur_demo.jpg", dst1)
    cv2.imshow("median_blur_demo", dst1)
    cv2.waitKey()

if __name__ == '__main__':
    main()
